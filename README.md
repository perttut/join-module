# Task 1

CommonJS module, which does simple array joining, duplicate removal and ordering by timestamp.

## Docs

Generate docs with the following command and see output in docs directory.
```bash
npm run jsdoc
```

## Test

```bash
npm test

```

## Lint

```bash
npm run lint
```
