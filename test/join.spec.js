'use strict';

const join = require('../src/join');

const createObj = (lastModified) => ({lastModified});
const extractLastModifieds = (objArr) => objArr.map((obj) => obj.lastModified);
const testDate1 = new Date();
const testDate2 = new Date(testDate1.getTime() - 1);
const testDate3 = new Date(testDate1.getTime() - 2);

const createSimpleArray = () => [createObj(testDate1), createObj(testDate2), createObj(testDate3)];

describe('join', function() {
  it('joins no array', () => {
    const result = join();
    expect(extractLastModifieds(result)).toEqual([]);
  });
  it('joins empty array', () => {
    const result = join([]);
    expect(extractLastModifieds(result)).toEqual([]);
  });
  it('joins array with one item', () => {
    const result = join([createObj(testDate1)]);
    expect(extractLastModifieds(result)).toEqual([testDate1]);
  });
  it('joins single array with non-duplicates', () => {
    const result = join(createSimpleArray());
    expect(extractLastModifieds(result)).toEqual([testDate3, testDate2, testDate1]);
  });
  it('joins two arrays with non-duplicates with same values', () => {
    const result = join(createSimpleArray(), createSimpleArray());
    expect(extractLastModifieds(result)).toEqual([testDate3, testDate3, testDate2, testDate2, testDate1, testDate1]);
  });
  it('joins single array with duplicates', () => {
    const simpleArray = createSimpleArray();
    const result = join([...simpleArray, simpleArray[0]]);
    expect(extractLastModifieds(result)).toEqual([testDate3, testDate2, testDate1]);
  });
  it('joins two array with duplicates', () => {
    const simpleArray = createSimpleArray();
    const result = join(simpleArray, simpleArray);
    expect(extractLastModifieds(result)).toEqual([testDate3, testDate2, testDate1]);
  });
});
