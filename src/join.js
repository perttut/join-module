'use strict';

/**
 * @module Join
 */

/**
 * Joins variable number of arrays into one, Removes possible duplicate objects and orders the returned array by
 * lastModified property with oldest lastModified first.
 *
 * @param   {Object[]} arrays     Array of arrays.
 * @return  {Object[]}            Ordered array of given objects with no duplicates.
 */
module.exports = (...arrays) => {
  const allObjectsArray = arrays.reduce((acc, curr) => [...acc, ...curr], []);
  const nonDuplicatesSet = allObjectsArray.reduce((acc, curr) => acc.add(curr), new Set());
  const orderedObjectsArray = Array
    .from(nonDuplicatesSet.values())
    .sort((a, b) => a.lastModified.getTime() - b.lastModified.getTime());
  return orderedObjectsArray;
};

